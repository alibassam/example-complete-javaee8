package web.tier.listeners;

import java.util.logging.Logger;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;

/**
 * This is a Servlet Context Attribute Listener to listen to context attributes changes
 * @author Ali Bassam
 * @email contact@alibassam.com
 * @project example-complete-javaee8-web-tier
 */
@WebListener
public class MyServletContextAttributeListener implements ServletContextAttributeListener {

	@Override
	public void attributeAdded(ServletContextAttributeEvent event) {
		ServletContextAttributeListener.super.attributeAdded(event);
		Logger.getLogger(MyServletContextAttributeListener.class.getSimpleName()).info("attributeAdded " + event.getName() + " of value " + event.getValue());
	}
	
	@Override
	public void attributeRemoved(ServletContextAttributeEvent event) {
		ServletContextAttributeListener.super.attributeRemoved(event);
		Logger.getLogger(MyServletContextAttributeListener.class.getSimpleName()).info("attributeRemoved " + event.getName() + " of value " + event.getValue());
	}
	
	@Override
	public void attributeReplaced(ServletContextAttributeEvent event) {
		ServletContextAttributeListener.super.attributeReplaced(event);
		Logger.getLogger(MyServletContextAttributeListener.class.getSimpleName()).info("attributeReplaced " + event.getName() + " of value " + event.getValue());
	}
}

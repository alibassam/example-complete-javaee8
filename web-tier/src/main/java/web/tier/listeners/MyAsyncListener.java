package web.tier.listeners;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.annotation.WebListener;

/**
 * This is an Async Listener to listen to all Async Operations
 * @author Ali Bassam
 * @email contact@alibassam.com
 * @project example-complete-javaee8-web-tier
 */
@WebListener
public class MyAsyncListener implements AsyncListener {

	@Override
	public void onComplete(AsyncEvent event) throws IOException {
		Logger.getLogger(MyAsyncListener.class.getSimpleName()).info("onComplete " + event.getSuppliedRequest());
	}

	@Override
	public void onTimeout(AsyncEvent event) throws IOException {
		Logger.getLogger(MyAsyncListener.class.getSimpleName()).info("onTimeout " + event.getSuppliedRequest());
	}

	@Override
	public void onError(AsyncEvent event) throws IOException {
		Logger.getLogger(MyAsyncListener.class.getSimpleName()).info("onError " + event.getSuppliedRequest());
	}

	@Override
	public void onStartAsync(AsyncEvent event) throws IOException {
		Logger.getLogger(MyAsyncListener.class.getSimpleName()).info("onStartAsync " + event.getSuppliedRequest());
	}

}

package web.tier.listeners;

import java.util.logging.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * This is an HTTP Session Listener to listen to all Sessions
 * @author Ali Bassam
 * @email contact@alibassam.com
 * @project example-complete-javaee8-web-tier
 */
@WebListener
public class MyHttpSessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		HttpSessionListener.super.sessionCreated(se);
		Logger.getLogger(MyHttpSessionListener.class.getSimpleName()).info("sessionCreated " + se.getSession().getId());
	}
	
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		HttpSessionListener.super.sessionDestroyed(se);
		Logger.getLogger(MyHttpSessionListener.class.getSimpleName()).info("sessionDestroyed " + se.getSession().getId());
	}
}

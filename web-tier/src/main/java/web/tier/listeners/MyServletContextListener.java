package web.tier.listeners;

import java.util.logging.Logger;

import javax.faces.application.ViewVisitOption;
import javax.faces.context.FacesContext;
import javax.faces.webapp.FacesServlet;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.ServletRegistration.Dynamic;
import javax.servlet.annotation.WebListener;

/**
 * This is a Servlet Context Listener that listens to the events of 
 * the start up and destruction of the Servlet Context
 * @author Ali Bassam
 * @email contact@alibassam.com
 * @project example-complete-javaee8-web-tier
 */
@WebListener
public class MyServletContextListener implements ServletContextListener {
	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContextListener.super.contextInitialized(sce);
		//Registering the JSF Servlet
//		Dynamic facesServlet = sce.getServletContext().addServlet("FacesServlet", FacesServlet.class);
		//Specifying the Servlet Mapping
//		facesServlet.addMapping("*.xhtml");
		//Setting Priority, 0 or higher for eager, if negative then it's lazy
//		facesServlet.setLoadOnStartup(1);
		//The previous lines were commented because it seems JSF 2.3 registers itself under the name of FacesServlet
		//with default mapping equal to:
		// /faces/*
		// *.faces
		// *.jsf
		// *.xhtml
		Logger.getLogger(MyServletContextListener.class.getSimpleName()).info("contextInitialized " + sce.getServletContext().getContextPath());
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		ServletContextListener.super.contextDestroyed(sce);
		Logger.getLogger(MyServletContextListener.class.getSimpleName()).info("contextDestroyed " + sce.getServletContext().getContextPath());
	}
}

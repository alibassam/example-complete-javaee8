package web.tier.listeners;

import java.util.logging.Logger;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * This is a Servlet Request Listener to listen to all incoming 
 * requests and outgoing responses
 * @author Ali Bassam
 * @email contact@alibassam.com
 * @project example-complete-javaee8-web-tier
 */
public class MyServletRequestListener implements ServletRequestListener {
	@Override
	public void requestInitialized(ServletRequestEvent sre) {
		ServletRequestListener.super.requestInitialized(sre);
		Logger.getLogger(MyServletRequestListener.class.getSimpleName()).info("requestInitialized " + sre.getServletRequest());
	}

	@Override
	public void requestDestroyed(ServletRequestEvent sre) {
		ServletRequestListener.super.requestDestroyed(sre);
		Logger.getLogger(MyServletRequestListener.class.getSimpleName()).info("requestDestroyed " + sre.getServletRequest());
	}
}
